package com.example.GPSLocator;

import java.util.List;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;

public class GPSLocatorActivity extends MapActivity {
	
	private LocationManager locationMgr;
    private LocationListener locationListener;
	private Handler handler;
	private Runnable handlerCallback;
	private ProgressDialog progressDialog;
	private MapController mapController;
	private MapView mapView;
	private static final int LOCATION_LISTEN_WAIT_TIME = 20000; 
	private static final int FIX_RECENT_BUFFER_TIME = 30000;
	private static final String TAG = "GPSLocatorActivity"; 
	private MyMyLocationOverlay myLocationOverlay;
	public DisplayOverlay displayOverlay;
	private List<Overlay> mapOverlays;
	private static double lat = 35.955;
	private static double lon = -83.9265;
	float satAccuracy = 2000;
	private float bearing;
	private double altitude;
	private float speed;
	Bundle locBundle;
	int numberSats;
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapview);
        
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Checking for current location...");
        Button myButton;
        final Dialog myDialog = new Dialog(GPSLocatorActivity.this);
        
        mapView = (MapView) findViewById(R.id.mapView);   
        
        // enable to show Satellite view
        // mapView.setSatellite(true);
        
        // enable to show Traffic on map
        // mapView.setTraffic(true);
        
        // Enable In Zoom Controls on Map
        mapView.setBuiltInZoomControls(true);
        
        // Get Mapp Controls and set Zoom
        mapController = mapView.getController();
        mapController.setZoom(16); 
        
        // Set up compass and dot for present location map overlay
        List<Overlay> overlays = mapView.getOverlays();
        myLocationOverlay = new MyMyLocationOverlay(this,mapView);
        overlays.add(myLocationOverlay);   
        
        // Set up overlay for data display
        displayOverlay = new DisplayOverlay();
        mapOverlays = mapView.getOverlays();
        mapOverlays.add(displayOverlay);

        
        locationMgr = (LocationManager) getSystemService(Context.LOCATION_SERVICE); 
        
        // 1. setup Handler and callback (will be used to stop location listener at specified duration)
        handler = new Handler();
        handlerCallback = new Runnable() {
           public void run() {
              endListenForLocation("Unable to determine current location.");
           }
        };
        
        // 2. get provider 
        Criteria criteria = new Criteria();
        // use Criteria to get provider (and could use COARSE, but doesn't work in emulator)
        // (FINE will use EITHER network/gps, whichever is the best enabled match, except in emulator must be gps)
        // (NOTE: network won't work unless enabled - Settings->Location & Security Settings->Use wireless networks)
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        String providerName = locationMgr.getBestProvider(criteria, false);
        
        // 3. get handle on loc listener
        locationListener = new LocationListenerImpl();
        
     // 4. if provider not null, use last known location (if recent), or listen for updates
        if (providerName != null) {
           // first check last KNOWN location
           Location lastKnown = locationMgr.getLastKnownLocation(providerName);
           // if the lastKnownLocation is present (often WON'T BE) and within the last X seconds, just use it         
           // NOTE -- this does NOT WORK in the Emulator
           // if you send a DDMS "manual" time or geo fix, you get correct DATE, but fix time starts at 00:00 and increments by 1 second each time sent
           // to test this section (getLastLocation being recent enough), you need to use a real device
           // http://stackoverflow.com/questions/4889487/android-emulators-gps-location-gives-wrong-time
           if (lastKnown != null && lastKnown.getTime() >= (System.currentTimeMillis() - FIX_RECENT_BUFFER_TIME)) {
        	   Toast.makeText(getBaseContext(), "Current location (taken from last known using " + providerName + " provider): "
                       + lastKnown.getLatitude() + " / " + lastKnown.getLongitude(),Toast.LENGTH_SHORT).show();
           } else {
              // last known is relatively old, or doesn't exist, use a LocationListener 
              // wait for a good location update for X seconds
              listenForLocation(providerName, LOCATION_LISTEN_WAIT_TIME);
           }
        } else {
        	Toast.makeText(getBaseContext(), "ACCURACY_COARSE (or above) location provider not available, unable to determine current location.",Toast.LENGTH_SHORT).show();
        } 
        
        myButton = (Button) findViewById(R.id.clickBtn);
        
        myButton.setOnClickListener(new OnClickListener() {        	 
        @Override
            public void onClick(View v) {
	        	myDialog.setContentView(R.layout.mydialog);
	        	myDialog.setTitle("About to Share Location");
	        	myDialog.setCancelable(true);
                Button button1 = (Button) myDialog.findViewById(R.id.Btn1);
                Button button2 = (Button) myDialog.findViewById(R.id.Btn2);
                
                
                OnClickListener buttonClickListener = new OnClickListener() {        	
                      @Override
                      public void onClick(View v) {
                    	  double mylat;
                    	  double mylon;
                          Criteria criteria = new Criteria();
                          criteria.setAccuracy(Criteria.ACCURACY_FINE);
                          String providerName = locationMgr.getBestProvider(criteria, false);
                    	  Location lastKnown = locationMgr.getLastKnownLocation(providerName);

                    	  if(lastKnown != null && !Double.isNaN(lastKnown.getLatitude()) && !Double.isNaN(lastKnown.getLongitude())) {
                    		   mylat = lastKnown.getLatitude();
                    		   mylon = lastKnown.getLongitude();
                    	  }
                    	  else
                    	  {
                    		  mylat = lat;
                    		  mylon = lon;
                    	  }
                          int id = v.getId();
                          
                    	  switch (id) {  
                    	    case R.id.Btn1:  
                    	    	myDialog.dismiss();
                    	        break;
                    	    case R.id.Btn2: 
                    	    	Intent i = new Intent(GPSLocatorActivity.this, ShowTheMap.class);
                    	    	
                                ShowTheMap.putLatLong(mylat, mylon);
                                startActivity(i);
                    	    	break;
                        }
                      }
                };
                
                button1.setOnClickListener(buttonClickListener);
                button2.setOnClickListener(buttonClickListener);
                
 
                myDialog.show();
            }
        });
    }
        
    @Override
    protected void onPause() {
       super.onPause();
       if (progressDialog.isShowing()) {
          progressDialog.hide();
       }
       
       Log.i(TAG,"******  GPSLocatorActivity is pausing: Removing GPS update requests to save power");
       myLocationOverlay.disableCompass();
       myLocationOverlay.disableMyLocation();
       locationMgr.removeUpdates(locationListener);
    }    
    
    @Override
    protected void onResume(){
        super.onResume();
        
        // 2. get provider 
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        String providerName = locationMgr.getBestProvider(criteria, false);
        
        Log.i(TAG,"******  GPSLocatorActivity is restarting: Resuming GPS update requests");	   
        locationMgr.requestLocationUpdates(providerName,0,0,locationListener);	
        myLocationOverlay.enableCompass();
        myLocationOverlay.enableMyLocation();
    }
    
 // TODO leaks ProgressDialog
    private void listenForLocation(String providerName, int duration) {
       progressDialog.show();
       locationMgr.requestLocationUpdates(providerName, 0, 0, locationListener);
       handler.postDelayed(handlerCallback, duration);
   
    }
    
    private void endListenForLocation(String message) {
        progressDialog.hide();
        locationMgr.removeUpdates(locationListener);
        handler.removeCallbacks(handlerCallback);
        Toast.makeText(getBaseContext(), message,Toast.LENGTH_SHORT).show();
    }
    
 // Method to query phone location and center map on that location
    private void centerOnLocation(Location location) {
    	 //  get provider 
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        String providerName = locationMgr.getBestProvider(criteria, false);
    	
    	Location loc = locationMgr.getLastKnownLocation(providerName);
    	if(loc != null){
    		 Toast.makeText(getBaseContext(), 
	                   "Latitude: " + location.getLatitude() + 
	                   " Longitude: " + location.getLongitude(), 
	                   Toast.LENGTH_SHORT).show();
    		 
            lat = loc.getLatitude();
            lon = loc.getLongitude();
            GeoPoint newPoint = new GeoPoint((int)(lat*1e6),(int)(lon*1e6)); 	   
            mapController.animateTo(newPoint);
            mapController.setZoom(16);
            mapView.invalidate();
            getSatelliteData();
        }
    	
    	if(displayOverlay != null){
            displayOverlay.putSatStuff(lat, lon, satAccuracy, bearing, 
               altitude, speed, providerName, numberSats);
        }
    	
    	 Toast.makeText(getBaseContext(), 
                 "Latitude: " + location.getLatitude() + 
                 " Longitude: " + location.getLongitude(), 
                 Toast.LENGTH_SHORT).show();

    }
    
    // Method to determine the number of satellites contributing to the fix and
    // various other quantities.
    
    public void getSatelliteData(){
    	 //  get provider 
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        String providerName = locationMgr.getBestProvider(criteria, false);
    	
    	Location loc = locationMgr.getLastKnownLocation(providerName);
    	
    	if(loc != null){
    		
            // Determine number of satellites used for the fix
            locBundle = loc.getExtras();
            if(locBundle != null){ 
                numberSats = locBundle.getInt("satellites",-1);
            }
	   	    
           // Following return 0 if the corresponding boolean (e.g., hasAccuracy) are false.
            satAccuracy = loc.getAccuracy();
            bearing = loc.getBearing();
            altitude = loc.getAltitude();
            speed = loc.getSpeed();		
        }
    }

    
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	// This class subclasses (extends) MyLocationOverlay so that 
    // we can override its dispatchTap method
    // to handle tap events on the present location dot.
    
    public class MyMyLocationOverlay extends MyLocationOverlay {
    
        private Context context;
    
        public MyMyLocationOverlay(Context context, MapView mapView) {
            super(context, mapView);
            this.context = context;   
        }
            
        // Add stub to Override the dispatchTap() method. Will fill in below. 
        
        @Override
        protected boolean dispatchTap(){
//        	if(DisplayOverlay.showData){ 
//                Toast.makeText(context, "Suppressing data readout", Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(context,"Display data readout", Toast.LENGTH_SHORT).show();
//            }
//            // Toggle the GPS data display
//            DisplayOverlay.showData = ! DisplayOverlay.showData;
            return true;
        }
    }
	
	private class LocationListenerImpl implements LocationListener
	{
	       private LocationListenerImpl()
	       {
	       }

	       @Override
	       public void onLocationChanged(Location location) 
	       {       
	    	   if (location != null) {
	    		   centerOnLocation(location);
	    	   }
	    	   else {
	    		   Toast.makeText(getBaseContext(),"Null Value",Toast.LENGTH_SHORT).show();
	    	   }
	               
	       }

	       @Override
	       public void onProviderDisabled(String provider) 
	       {       
	    	   endListenForLocation("No suitable location provider available, chosen provider is DISABLED (" + provider + ").");
	           Toast.makeText(GPSLocatorActivity.this, "provider disabled", Toast.LENGTH_SHORT).show();
	        
	       }

	       @Override
	       public void onProviderEnabled(String provider) 
	       {       
	    	   Toast.makeText(GPSLocatorActivity.this, "provider enabled", Toast.LENGTH_SHORT).show();
	       }

	       @Override
	       public void onStatusChanged(String provider, int status, Bundle extras) {
	          Log.d("LocationListener", "Status changed to " + status);
	          String message = "Status changed to: ";
	          switch (status) {
	             case LocationProvider.AVAILABLE:
	                message += status + " " + "AVAILABLE";
	                break;
	             case LocationProvider.TEMPORARILY_UNAVAILABLE:
	                message += status + " " + "TEMPORARILY_UNAVAILABLE";
	                break;
	             case LocationProvider.OUT_OF_SERVICE:
	                message += status + " " + "OUT_OF_SERVICE";               
	                endListenForLocation("Location provider OUT OF SERVICE, unable to determine location at the current time.");
	          }
	          Log.d("GetCurrentLocation", "Status Change " + message);
	          Location loc = locationMgr.getLastKnownLocation(provider);
	          centerOnLocation(loc);
	       }

		

		
	}
}