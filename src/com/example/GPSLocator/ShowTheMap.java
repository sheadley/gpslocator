package com.example.GPSLocator;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import java.util.List;


import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import android.graphics.drawable.Drawable;


public class ShowTheMap extends MapActivity {
    
    private static double lat;
    private static double lon;
    private int latE6;
    private int lonE6;
    private MapController mapControl;
    private GeoPoint gp;
    private MapView mapView;
    private List<Overlay> mapOverlays;
    private Drawable drawable1, drawable2;
    private MyItemizedOverlay itemizedOverlay1, itemizedOverlay2;
    
    private CustomItemizedOverlay<CustomOverlayItem> itemizedOverlay;
	private CustomItemizedOverlay<CustomOverlayItem> itemizedOverlayTwo;
    private boolean foodIsDisplayed = false;
    
    // Define an array containing the food overlay items
    
//    private OverlayItem [] foodItem = {
//        new OverlayItem( new GeoPoint(35952967,-83929158), "Food Title 1", "Food snippet 1"), 
//        new OverlayItem( new GeoPoint(35953000,-83928000), "Food Title 2", "Food snippet 2"),
//        new OverlayItem( new GeoPoint(35955000,-83929158), "Food Title 3", "Food snippet 3") 
//    };
    
  private CustomOverlayItem [] foodItem = {
		  new CustomOverlayItem( new GeoPoint(38996588,-77022705), "Whole Foods1", "Venue snippet 1", "Venue snippet2 1"), 
		  new CustomOverlayItem( new GeoPoint(38995340,-77027421), "Panera Bread 2", "Food snippet 2", "Venue snippet2 2"),
		  new CustomOverlayItem( new GeoPoint(38993822, -77031793), "MacDonalds 3", "Food snippet 3", "Venue snippet2 3") 
};
    
    // Define an array containing the  access overlay items
    
//    private OverlayItem [] accessItem = {
//        new OverlayItem( new GeoPoint(35953700,-83926158), "Access Title 1", "Access snippet 1"),
//        new OverlayItem( new GeoPoint(35954000,-83928200), "Access Title 2", "Access snippet 2"),
//        new OverlayItem( new GeoPoint(35955000,-83927558), "Access Title 3", "Access snippet 3"),
//        new OverlayItem( new GeoPoint(35954000,-83927158), "Access Title 4", "Access snippet 4") 
//    };
  
  	  private CustomOverlayItem [] accessItem = {
	        new CustomOverlayItem( new GeoPoint(35953700,-83926158), "Access Title 1", "Access snippet 1", "Access snippet1 1"),
	        new CustomOverlayItem( new GeoPoint(35954000,-83928200), "Access Title 2", "Access snippet 2", "Access snippet1 2"),
	        new CustomOverlayItem( new GeoPoint(35955000,-83927558), "Access Title 3", "Access snippet 3", "Access snippet1 3"),
	        new CustomOverlayItem( new GeoPoint(35954000,-83927158), "Access Title 4", "Access snippet 4", "Access snippet1 4") 
	    };
    
    private Button overlayButton, accessButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);  // Suppress title bar for more space
        setContentView(R.layout.showthemap);
        
        // Add map controller with zoom controls
        mapView = (MapView) findViewById(R.id.mv);
        mapView.setSatellite(false);
        mapView.setTraffic(false);
        mapView.setBuiltInZoomControls(true);   // Set android:clickable=true in main.xml

        int maxZoom = mapView.getMaxZoomLevel();
        int initZoom = maxZoom-2;

        mapControl = mapView.getController();
        mapControl.setZoom(initZoom);
        // Convert lat/long in degrees into integers in microdegrees
        latE6 =  (int) (lat*1e6);
        lonE6 = (int) (lon*1e6);
        gp = new GeoPoint(latE6, lonE6);
        mapControl.animateTo(gp);    

        // Button to control food overlay
        overlayButton = (Button)findViewById(R.id.doOverlay);
        overlayButton.setOnClickListener(new OnClickListener(){      
            public void onClick(View v) {	
            	setOverlay1(); 
            }
        });
        
        // Button to control access overlay
        accessButton = (Button)findViewById(R.id.doAccess);
        accessButton.setOnClickListener(new OnClickListener(){      
            public void onClick(View v) {	
            	setOverlay2();  
            }
        });

    }
    
    /* Methods to set map overlays. In this case we will place a small overlay image at
    a specified location. Place the marker image as a png file in res > drawable-* .
    For example, the reference to R.drawable.knifefork_small below is to an
    image file called knifefork_small.png in the project folder res > drawable-hdpi.
    Can only use lower case letters a-z, numbers 0-9, ., and _ in these image file names. 
    In this example the single overlay item is specified by drawable and the
    location of the overlay item is specified by overlayitem. */
    
    // Display food location overlay.  If not already displayed, clicking button displays all 
    // food overlays.  If already, displayed successive clicks remove items one by one. This
    // illustrates ability to remove individual overlay items dynamically at runtime.
    
    public void setOverlay1(){	
        int foodLength = foodItem.length;
        // Create itemizedOverlay2 if it doesn't exist and display all three items
        if(! foodIsDisplayed){
        mapOverlays = mapView.getOverlays();	
        drawable1 = this.getResources().getDrawable(R.drawable.knife_fork_logo_tn); 
 //       itemizedOverlay1 = new MyItemizedOverlay(getApplicationContext(), drawable1); 
        itemizedOverlay = new CustomItemizedOverlay(drawable1, mapView);
        // Display all three items at once
        for(int i=0; i<foodLength; i++){
//            itemizedOverlay1.addOverlay(foodItem[i]);
        	itemizedOverlay.addOverlay(foodItem[i]);  
        }
//        mapOverlays.add(itemizedOverlay1);
        mapOverlays.add(itemizedOverlay);
        foodIsDisplayed = !foodIsDisplayed;
        // Remove each item successively with button clicks
        } else {			
//            itemizedOverlay1.removeItem(itemizedOverlay1.size()-1);
//        	itemizedOverlay.removeItem(itemizedOverlay.size()-1); -- not sure must check this code.
            if((itemizedOverlay.size() < 1))  foodIsDisplayed = false;
        }    
        // Added symbols will be displayed when map is redrawn so force redraw now
        mapView.postInvalidate(); 
    }
    
    // Display accessibility overlay.  If not already displayed, successive button clicks display each of
    // the three icons successively, then the next removes them all.  This illustrates the ability to add
    // individual overlay items dynamically at runtime
    
    public void setOverlay2(){	
        int accessLength = accessItem.length;
        // Create itemizedOverlay2 if it doesn't exist
        if(itemizedOverlayTwo == null ){
        mapOverlays = mapView.getOverlays();	
        drawable2 = this.getResources().getDrawable(R.drawable.dj_tn);
//        itemizedOverlay2 = new MyItemizedOverlay(getApplicationContext(), drawable2);
        itemizedOverlayTwo = new CustomItemizedOverlay(drawable2, mapView);
        }     
        // Add items with each click
        if(itemizedOverlayTwo.size() < accessLength){
 //               itemizedOverlay2.addOverlay(accessItem[itemizedOverlay2.size()]); 	
        	    itemizedOverlayTwo.addOverlay(accessItem[itemizedOverlayTwo.size()]); 	 
                mapOverlays.add(itemizedOverlayTwo);      
        // Remove all items with one click
        } else {
            for(int i=0; i<accessLength; i++){
 //               itemizedOverlay2.removeItem(accessLength-1-i);
            }
        }     
        // Added symbols will be displayed when map is redrawn so force redraw now
        mapView.postInvalidate();
    }

    // Method to insert latitude and longitude in degrees
    public static void putLatLong(double latitude, double longitude){
        lat = latitude;
        lon =longitude;
    }
    
    // This sets the s key on the phone to toggle between satellite and map view
    // and the t key to toggle between traffic and no traffic view (traffic view
    // relevant only in urban areas where it is reported).
    
    public boolean onKeyDown(int keyCode, KeyEvent e){
        if(keyCode == KeyEvent.KEYCODE_S){
            mapView.setSatellite(!mapView.isSatellite());
            return true;
        } else if(keyCode == KeyEvent.KEYCODE_T){
            mapView.setTraffic(!mapView.isTraffic());
            mapControl.animateTo(gp);  // To ensure change displays immediately
        }
            return(super.onKeyDown(keyCode, e));
    }
                        
    // Required method since class extends MapActivity
    @Override
    protected boolean isRouteDisplayed() {
            return false;  // Don't display a route
    }
}